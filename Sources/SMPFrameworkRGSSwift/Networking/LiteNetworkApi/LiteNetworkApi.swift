////
////  File.swift
////
////
////  Created by Artem Ermochenko on 1/25/22.
////
//
//import Foundation
//
//typealias JSONDictionary = Dictionary<String, AnyObject>
//typealias JSONArray = Array<AnyObject>
//
//class API: NSObject, NSURLConnectionDataDelegate {
//
//    enum Path {
//        case USER_REFRESH //user/refresh
//    }
//
//    typealias APICallback = ((AnyObject?, NSError?) -> ())
//    let responseData = NSMutableData()
//    var statusCode:Int = -1
//    var callback: APICallback! = nil
//    var path: Path! = nil
//
//    func userRefresh(host: String, path: Path, callback: APICallback) {
//        let url = host
//        makeHTTPPostRequest(Path.USER_REFRESH, callback: callback, url: url)
//    }
//
//    func connection(connection: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
//        let httpResponse = response as NSHTTPURLResponse
//        statusCode = httpResponse.statusCode
//        switch (httpResponse.statusCode) {
//        case 201, 200, 401:
//            self.responseData.length = 0
//        default:
//            println("ignore")
//        }
//    }
//
//    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
//        self.responseData.appendData(data)
//    }
//
//    func connectionDidFinishLoading(connection: NSURLConnection!) {
//        var error: NSError?
//        var json : AnyObject! = NSJSONSerialization.JSONObjectWithData(self.responseData, options: NSJSONReadingOptions.MutableLeaves, error: &error)
//        if error {
//            callback(nil, error)
//            return
//        }
//
//        switch(statusCode, self.path!) {
//        case (200, Path.USER_REFRESH):
//            callback(self.handleUserRefresh(json), nil)
//        case (401, _):
//            callback(nil, handleAuthError(json))
//        default:
//            // Unknown Error
//            callback(nil, nil)
//        }
//    }
//
//    func handleAuthError(json: AnyObject) -> NSError {
//        if let resultObj = json as? JSONDictionary {
//            // beta2 workaround
//            if let messageObj: AnyObject = resultObj["error"] {
//                if let message = messageObj as? String {
//                    return NSError(domain:"signIn", code:401, userInfo:["error": message])
//                }
//            }
//        }
//        return NSError(domain:"signIn", code:401, userInfo:["error": "unknown auth error"])
//    }
//
//    func handleUserRefresh(json: AnyObject) -> User? {
//        if let resultObj = json as? JSONDictionary {
//            if let userObj: AnyObject = resultObj["user"] {
//                if let userJson = userObj as? JSONDictionary {
//                    if let user = User.createFromJson(userJson) {
//                        return user
//                    }
//                }
//            }
//        }
//        return nil
//    }
//
//    //    func handleCreateMessage(json: AnyObject) -> Message? {
//    //        if let messageObject = json as? JSONDictionary {
//    //            return Message.createFromJson(messageObject)
//    //        } else {
//    //            return nil
//    //        }
//    //    }
//    //
//    //    func handleGetRooms(json: AnyObject) -> Array<Room> {
//    //        var rooms = Array<Room>()
//    //        if let roomObjects = json as? JSONArray {
//    //            for roomObject: AnyObject in roomObjects {
//    //                if let roomJson = roomObject as? JSONDictionary {
//    //                    if let room = Room.createFromJson(roomJson) {
//    //                        rooms.append(room)
//    //                    }
//    //                }
//    //            }
//    //        }
//    //        return rooms;
//    //    }
//    //
//    //    func handleGetMessages(json: AnyObject) -> Array<Message> {
//    //        var messages = Array<Message>()
//    //        if let messageObjects = json as? JSONArray {
//    //            for messageObject: AnyObject in messageObjects {
//    //                if let messageJson = messageObject as? JSONDictionary {
//    //                    if let message = Message.createFromJson(messageJson) {
//    //                        messages.append(message)
//    //                    }
//    //                }
//    //            }
//    //        }
//    //        return messages;
//    //    }
//
//    // private
//    func makeHTTPGetRequest(path: Path, callback: APICallback, url: NSString) {
//        self.path = path
//        self.callback = callback
//        let request = NSURLRequest(URL: NSURL(string: url))
//        let conn = NSURLConnection(request: request, delegate:self)
//        if (conn == nil) {
//            callback(nil, nil)
//        }
//    }
//
//    func makeHTTPPostRequest(path: Path, callback: APICallback, url: NSString, body: NSString) {
//        self.path = path
//        self.callback = callback
//        let request = NSMutableURLRequest(URL: NSURL(string: url))
//        request.HTTPMethod = "POST"
//        request.HTTPBody = body.dataUsingEncoding(NSUTF8StringEncoding)
//        let conn = NSURLConnection(request: request, delegate:self)
//        if (conn == nil) {
//            callback(nil, nil)
//        }
//    }
//
//    func makeHTTPPostRequest(path: Path, callback: APICallback, url: NSString, body: NSString) {
//        let params = ["username":"john", "password":"123456"] as Dictionary<String, String>
//        var request = URLRequest(url: URL(string: "http://localhost:8080/api/1/login")!)
//        request.httpMethod = "POST"
//        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        let session = URLSession.shared
//        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
//            print(response!)
//            do {
//                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
//                print(json)
//            } catch {
//                print("error")
//            }
//        })
//
//        task.resume()
//
//
//        let params = ["username":"john", "password":"123456"] as Dictionary<String, String>
//
//        var request = URLRequest(url: URL(string: "http://example.com/api/v1/example")!)
//        request.httpMethod = "POST"
//        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//
//        URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
//            do {
//                let jsonDecoder = JSONDecoder()
//                let responseModel = try jsonDecoder.decode(CustomDtoClass.self, from: data!)
//                print(responseModel)
//            } catch {
//                print("JSON Serialization error")
//            }
//        }).resume()
//    }
//}
