# SMPFrameworkRGS

This is SMPFrameworkRGS framework for iOS.

## Installation

### Swift Package Manager 

Requires Swift 5.3 / Xcode 12+.

Add SMPFrameworkRGS repository https://artemErm@bitbucket.org/artemErm/smpframergs.git via Swift Package Manager  

Alternatively, to integrate via a Package.swift manifest instead of Xcode, you can add SMPFrameworkRGS to your dependencies array of your package with

```swift
dependencies: [
    .package(url: "https://artemErm@bitbucket.org/artemErm/smpframergs.git", .upToNextMajor(from: "1.0.22"))
]
```
